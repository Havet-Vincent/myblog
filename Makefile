# Executables (local)
user := $(shell id -u)
group := $(shell id -g)
DOCKER=docker
DOCKER_COMPOSE=USER_ID=$(user) GROUP_ID=$(group) docker compose

# colors
GREEN = /bin/echo -e "\x1b[35m\#\# $1\x1b[0m"
RED = /bin/echo -e "\x1b[31m\#\# $1\x1b[0m"


# Docker containers
DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE) exec
PHP_DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE_EXEC) php

# Executables
COMPOSER=$(PHP_DOCKER_COMPOSE_EXEC) php -d memory_limit=-1 /usr/bin/composer
SYMFONY_CONSOLE=$(PHP_DOCKER_COMPOSE_EXEC) /usr/local/bin/symfony console
SYMFONY=$(PHP_DOCKER_COMPOSE_EXEC) /usr/local/bin/symfony

# Misc
.DEFAULT_GOAL = help
.PHONY: help 

## —— 🎵 🐳 The Symfony Docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳  ———————————————————————————————————————————————————————————————
build:	## Build and start the containers and  start the Symfony server
	@$(DOCKER_COMPOSE) up -d --build --pull --no-cache
	@$(SYMFONY) server:stop
	@$(SYMFONY) serve -d --port=$(shell cat .env | grep NGINX_PORT_SSL= | cut -d '=' -f2) 
	make extraInfos
	@$(call GREEN,"The containers and local server are now started.")

start:	## Start the containers and  start the Symfony server
	@$(DOCKER_COMPOSE) up -d --pull --no-cache
	@$(SYMFONY) serve -d --port=$(shell cat .env | grep NGINX_PORT_SSL= | cut -d '=' -f2) 
	make extraInfos
	@$(call GREEN,"The containers and local server are now started.")

stop:	## Stop the Symfony server and the docker containers
	@$(SYMFONY) server:stop
	@$(DOCKER_COMPOSE) stop
	@$(call RED,"The containers are now stopped.")

rm:	stop ## Stop the Symfony server and delete docker containers
	@$(DOCKER_COMPOSE) rm -f
	@$(call RED,"The all containers are now removed.")

rebuild: rm build	## Rebuild the docker containers and start the Symfony server 

sh:	## Connect to the PHP FPM container
	@$(PHP_DOCKER_COMPOSE_EXEC) sh

prune:      ## Remove all unused containers, networks, images, and optionally, volumes.
	@$(DOCKER) system prune -a -f
	@$(call RED,"All are now removed.")

## —— Composer 🧙 ——————————————————————————————————————————————————————————————
composer: ## Run composer, pass the parameter "c=" to run a given command, ex: make composer c='req symfony/orm-pack'
	@$(eval c ?=)
	@$(COMPOSER) $(c)

vendor-ins:	## Install vendors
	@$(COMPOSER) install

vendor-upd:	## Update vendors
	@$(COMPOSER) update

vendor-del: cc-hard ## Remove vendors and reinstall them
	@$(PHP_DOCKER_COMPOSE_EXEC) rm -Rf vendor
	@$(COMPOSER) install


## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
cc:	## Clear the cache
	@$(SYMFONY_CONSOLE) cache:clear

cc-hard: ## Remove the cache directory
	@$(PHP_DOCKER_COMPOSE_EXEC) rm -fR var/cache/*

c-db: ## Reinstall the database
	@$(SYMFONY_CONSOLE) d:d:d --force --connection
	@$(SYMFONY_CONSOLE) d:d:c
	@$(SYMFONY_CONSOLE) d:m:m --no-interaction
	@$(SYMFONY_CONSOLE) d:f:l --no-interaction


## —— Tests 🧪 ———————————————————————————————————————————————————————————————
cc-test: ## Clear the cache of the test environment
	@$(SYMFONY_CONSOLE) c:c --env=test

c-db-test: cc-hard cc-test ## Reset the database in test environment
	- @$(SYMFONY_CONSOLE) d:d:d --force --env=test
	@$(SYMFONY_CONSOLE) d:d:c --env=test
	@$(SYMFONY_CONSOLE) d:m:m --no-interaction --env=test
	@$(SYMFONY_CONSOLE) d:f:l --no-interaction --env=test

test-unit: ## Launch unit tests
	@$(PHP_DOCKER_COMPOSE_EXEC) bin/phpunit tests/Unit/

test-func: clean-db-test	## Launch functional tests
	@$(PHP_DOCKER_COMPOSE_EXEC) bin/phpunit tests/Func/

tests: test-func test-unit	## Launch all tests


## —— Inspection 🛃 ———————————————————————————————————————————————————————————————
phpcs: ## Run php-cs-filter
	$(PHP_DOCKER_COMPOSE_EXEC) vendor/bin/php-cs-fixer fix --allow-risky=yes

phpstan: ## Run phpstan 
	$(PHP_DOCKER_COMPOSE_EXEC) vendor/bin/phpstan analyse -c phpstan.neon

## ——————————————————————————————————————————————————————————————————————————————————
extraInfos:
	@echo "\n\033[0;33m \342\232\231 DEV! \033[0m"
	@echo "\033[0;32mDevelopment server:\033[0m https://localhost:$(shell cat .env | grep NGINX_PORT_SSL= | cut -d '=' -f2)"
	@echo "\033[0;32mPhpMyAdmin:\033[0m http://localhost:$(shell cat .env | grep PHPMYADMIN_PORT= | cut -d '=' -f2)"
	@echo "- \033[0;34muser:\033[0m $(shell cat .env | grep MARIADB_USER= | cut -d '=' -f2)"
	@echo "- \033[0;34mpassword:\033[0m $(shell cat .env | grep MARIADB_PASSWORD= | cut -d '=' -f2)"
	@echo "\033[0;32mMailhog:\033[0m http://localhost:$(shell cat .env | grep MAILHOG_PORT_2= | cut -d '=' -f2)\n"




