<?php
declare(strict_types=1);
$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('var')
    ->exclude('code')
    ->exclude('snapshots')
    ->exclude('tmp')
;

return (new PhpCsFixer\Config())->setRules([
    '@Symfony' => true,
    'declare_strict_types' => true,
    'no_superfluous_phpdoc_tags' => false,
    'php_unit_fqcn_annotation' => false,
    'phpdoc_to_comment' => false,
    'phpdoc_order' => true,
    'yoda_style' => false,
    'native_function_invocation' => [         // https://cs.symfony.com/doc/rules/function_notation/native_function_invocation.html
        'include' => ['@compiler_optimized'],
        'scope' => 'namespaced',
        'strict' => true,
    ], ])
->setFinder($finder);
