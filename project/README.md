# Projet Symfony

## Création variable d'environnement

1. créer un fichier .env.local ou autre avec la variable

- JWT_SECRET='ChangeMoi!'

2. créer les clés ssl dans nginx/certs

- localhost.crt
- localhost.key
