<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Category;
use App\Entity\Post;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PostFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Post|null $post */
        $post = $options['data'] ?? null;

        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'minlength' => 2,
                    'maxlength' => 100,
                ],
                'label' => 'Titre de l\'article',
                'label_attr' => [
                    'class' => 'mt-4',
                ],
                'constraints' => [
                    new Assert\Length([
                        'min' => 2,
                        'minMessage' => 'Votre titre doit contenir au minimum {{ limit }} caractères.',
                        'max' => 4096,
                    ]),
                    new Assert\NotBlank([
                        'message' => 'Veuillez entrer un titre.',
                    ]),
                ],
            ])
            ->add('content', TextareaType::class, [
                'attr' => [
                    'minlength' => 50,
                    'style' => 'height: 250px',
                    'resize' => 'both',
                    'placeholder' => 'Votre article...',
                ],
                'label' => 'Contenu de l\'article',
                'label_attr' => [
                    'class' => 'mt-4',
                ],
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Veuillez entrer un contenu.',
                    ]),
                    new Assert\Length([
                        'min' => 50,
                        'minMessage' => 'Votre contenu doit contenir au minimum {{ limit }} caractères.',
                        'max' => 8092,
                    ]),
                ],
            ])
            ->add('isPublished', CheckboxType::class, [
                'label' => 'Publication',
                'required' => false,
            ]);

        $imageConstraints = [
            new Assert\Image([
                'maxSize' => '5M',
            ]),
        ];

        if (!$post->getImage()) {
            $imageConstraints[] = new Assert\NotNull([
                'message' => 'Veuillez ajouter une image.',
            ]);
        }

        $builder
            ->add('image', FileType::class, [
                'required' => false,
                'data_class' => null,
                'mapped' => false,
                'constraints' => $imageConstraints,
            ])
            ->add('category', EntityType::class, [
                'attr' => [
                    'class' => 'text-capitalize',
                ],
                'label' => 'Catégories',
                'class' => Category::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => false,
                'constraints' => [
                    new Assert\Count([
                        'min' => 1,
                        'minMessage' => 'Veuillez sélectionner au moins une catégorie.',
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
