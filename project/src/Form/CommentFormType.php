<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CommentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Votre email',
                'label_attr' => [
                    'class' => 'mt-4',
                ],
            ])
            ->add('nickname', TextType::class, [
                'attr' => [
                    'minlength' => 5,
                ],
                'label' => 'Votre pseudo',
                'label_attr' => [
                    'class' => 'mt-4',
                ],
            ])
            ->add('content', TextareaType::class, [
                'attr' => [
                    'minlength' => 10,
                ],
                'label' => 'Votre commentaire',
                'label_attr' => [
                    'class' => 'mt-4',
                ],
            ])
            ->add('rgpd', CheckboxType::class, [
                'label' => 'Vous devez accepter les conditions',
                'mapped' => false,
                'constraints' => [
                    new Assert\IsTrue([
                        'message' => 'Vous devez accepter nos conditions.',
                    ]),
                ],
            ])
            ->add('envoyer', SubmitType::class, [
                'label' => 'Envoyer le commentaire',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
