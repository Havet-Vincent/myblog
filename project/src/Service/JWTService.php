<?php

declare(strict_types=1);

namespace App\Service;

final class JWTService
{
    /**
     * @param array  $header
     * @param array  $payload
     * @param string $secret
     * @param int    $validity
     *
     * @return string
     */
    public function generateToken(
        array $header,
        array $payload,
        string $secret,
        int $validity = 7200
    ): string {
        if ($validity > 0) {
            $now = new \DateTimeImmutable();
            $exp = $now->getTimestamp() + $validity;

            $payload['iat'] = $now->getTimestamp();
            $payload['exp'] = $exp;
        }

        $base64Header = base64_encode(json_encode($header));
        $base64Payload = base64_encode(json_encode($payload));

        $base64Header = str_replace(['+', '/', '='], ['-', '_', ''], $base64Header);
        $base64Payload = str_replace(['+', '/', '='], ['-', '_', ''], $base64Payload);

        $secret = base64_encode($secret);

        $signature = hash_hmac('sha256', $base64Header.'.'.$base64Payload, $secret, true);

        $base64Signature = base64_encode($signature);

        $base64Signature = str_replace(['+', '/', '='], ['-', '_', ''], $base64Signature);

        $jwt = $base64Header.'.'.$base64Payload.'.'.$base64Signature;

        return $jwt;
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    public function isValidToken(string $token): bool
    {
        return preg_match(
            '/^[a-zA-Z0-9\-\_\=]+\.[a-zA-Z0-9\-\_\=]+\.[a-zA-Z0-9\-\_\=]+$/',
            $token
        ) === 1;
    }

    /**
     * @param string $token
     *
     * @return array
     */
    public function getPayload(string $token): array
    {
        $array = explode('.', $token);

        $payload = json_decode(base64_decode($array[1]), true);

        return (array) $payload;
    }

    /**
     * @param string $token
     *
     * @return array
     */
    public function getHeader(string $token): array
    {
        $array = explode('.', $token);

        $header = json_decode(base64_decode($array[0]), true);

        return (array) $header;
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    public function isExpiredToken(string $token): bool
    {
        $payload = $this->getPayload($token);

        $now = new \DateTimeImmutable();

        return $payload['exp'] < $now->getTimestamp();
    }

    /**
     * @param string $token
     * @param string $secret
     *
     * @return bool
     */
    public function isSignatureOfToken(string $token, string $secret): bool
    {
        $header = $this->getHeader($token);
        $payload = $this->getPayload($token);

        $verifToken = $this->generateToken($header, $payload, $secret, 0);

        return $token === $verifToken;
    }
}
