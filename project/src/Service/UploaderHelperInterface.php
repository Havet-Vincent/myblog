<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface UploaderHelperInterface
{
    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    public function uploadImage(UploadedFile $file): string;

    /**
     * @param string $file
     *
     * @return bool
     */
    public function deleteImage(string $file): bool;
}
