<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderHelper implements UploaderHelperInterface
{
    public const EXTENSIONS = ['jpeg', 'png', 'gif', 'jpg'];

    public function __construct(
        private string $uploadsAbsoluteDir,
        private string $liipImagineMiniatureAbsoluteDir,
        private string $liipImagineFullAbsoluteDir,
    ) {
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    public function uploadImage(UploadedFile $file): string
    {
        if (!$this->controlExtension($file)) {
            throw new \Exception('Format d\'image incorrect');
        }

        $filename = sprintf(
            '%s.%s',
            uniqid(),
            $file->getClientOriginalExtension()
        );

        try {
            $file->move($this->getUploadsAbsoluteDir(), $filename);
        } catch (FileException $e) {
            echo 'Exception found - '.$e->getMessage();
        }

        return $filename;
    }

    /**
     * @param string|null $file
     *
     * @return bool
     */
    public function deleteImage(?string $file): bool
    {
        // TODO condition à supprimer pour la prod.
        if ($file == 'placeholder.png') {
            return true;
        }

        $pathImageMiniatureLiipFile = $this->liipImagineMiniatureAbsoluteDir.'/'.$file;
        $pathImageFullLiipFile = $this->liipImagineFullAbsoluteDir.'/'.$file;
        $pathFile = $this->getUploadsAbsoluteDir().'/'.$file;

        if (file_exists($pathFile) &&
            file_exists($pathImageMiniatureLiipFile) &&
            file_exists($pathImageFullLiipFile)) {
            if (unlink($pathFile) &&
                unlink($pathImageMiniatureLiipFile) &&
                unlink($pathImageFullLiipFile)) {
                return true;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getUploadsAbsoluteDir(): string
    {
        return $this->uploadsAbsoluteDir;
    }

    private function controlExtension(object $file): bool
    {
        return \in_array($file->getClientOriginalExtension(), self::EXTENSIONS);
    }
}
