<?php

declare(strict_types=1);

namespace App\Validator\Constraints;

use Attribute;
use Symfony\Component\Validator\Constraint;

#[\Attribute]
class EmailMX extends Constraint
{
    public string $message = 'Aucun serveur mail n\'a été trouvé pour ce domaine.';
}
