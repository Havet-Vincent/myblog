<?php

declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class EmailMXValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof EmailMX) {
            throw new UnexpectedTypeException($constraint, EmailMX::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        $valueType = \strval($value);

        if (!\is_string($valueType)) {
            throw new UnexpectedValueException($value, 'string');
        }
        $domain = substr($valueType, strrpos($valueType, '@') + 1);

        if (!checkdnsrr($domain, 'MX')) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $valueType)
                ->addViolation();
        }
    }
}
