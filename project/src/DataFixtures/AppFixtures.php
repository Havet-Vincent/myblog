<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Factory\CategoryFactory;
use App\Factory\CommentFactory;
use App\Factory\PostFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        CategoryFactory::createMany(3);

        UserFactory::createOne([
            'email' => 'user@example.com',
            'isAgree' => true,
        ]);

        UserFactory::createOne([
            'email' => 'admin@example.com',
            'pseudo' => 'blogueur',
            'password' => 'admin',
            'isAgree' => true,
            'roles' => ['ROLE_ADMIN', 'ROLE_SUPER_ADMIN'],
        ]);
        UserFactory::createMany(10);

        PostFactory::new()
            ->many(100)
            ->create(function () {
                return [
                    'category' => CategoryFactory::randomRange(1, 2),
                    'user' => UserFactory::random(),
                    'image' => 'placeholder.png',
                ];
            });

        CommentFactory::new()
            ->many(100)
            ->create(function () {
                return [
                    'post' => PostFactory::random(),
                ];
            });

        $manager->flush();
    }
}
