<?php

declare(strict_types=1);

namespace App\Twig;

use App\Repository\CategoryRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function __construct(private CategoryRepository $categoryRepository)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('categoriesNavBar', [$this, 'categories']),
        ];
    }

    /**
     * @return array
     */
    public function categories(): array
    {
        return $this->categoryRepository->getAllCategoriesOrderByNameAsc();
    }
}
