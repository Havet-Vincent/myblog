<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Post>
 *
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @param int $value
     *
     * @return array<mixed>
     */
    public function getTheLastTenPostsOrderByDateDesc(int $value = 10): array
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $queryBuilder = $this->addOrderByPublishedPostQueryBuilder($queryBuilder);
        $queryBuilder = $this->addOrderByPostQueryBuilder($queryBuilder);
        $queryBuilder->setMaxResults($value);

        return (array) $queryBuilder
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string|null $search
     *
     * @return array<mixed>
     */
    public function getAllPostsOrderByDateDesc(?string $search = null): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        if ($search) {
            $queryBuilder = $this->addSearchQueryBuilder($queryBuilder, $search);
        }

        $queryBuilder = $this->addOrderByPublishedPostQueryBuilder($queryBuilder);
        $queryBuilder = $this->addOrderByPostQueryBuilder($queryBuilder);

        return (array) $queryBuilder
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $category
     *
     * @return array<mixed>
     */
    public function getAllPostsByCategoryOrderByDesc(?string $category): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        if ($category) {
            $queryBuilder->join('p.category', 'c')
                ->where("c.name = '$category'");
        }

        $queryBuilder = $this->addOrderByPublishedPostQueryBuilder($queryBuilder);
        $queryBuilder = $this->addOrderByPostQueryBuilder($queryBuilder);

        return (array) $queryBuilder
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $category
     * @param string $pseudo
     *
     * @return array <mixed>
     */
    public function getAllPostsByCategoryAndPseudo(string $category, string $pseudo): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $queryBuilder->join('p.user', 'u')
        ->andWhere("u.pseudo = '$pseudo'");

        $queryBuilder->join('p.category', 'c')
            ->andWhere("c.name = '$category'");

        $queryBuilder = $this->addOrderByPublishedPostQueryBuilder($queryBuilder);
        $queryBuilder = $this->addOrderByPostQueryBuilder($queryBuilder);

        return (array) $queryBuilder
        ->getQuery()
        ->getResult();
    }

    /**
     * @param string|null $pseudo
     * @param string|null $search
     *
     * @return array|null
     */
    public function getAllPublishedPostsByUserOrderByDesc(
        ?string $pseudo,
        ?string $search = null
    ): ?array {
        $queryBuilder = $this->createQueryBuilder('p');

        if ($pseudo) {
            $queryBuilder->join('p.user', 'u')
                ->andWhere("u.pseudo = '$pseudo'");
        }

        if ($search) {
            $queryBuilder = $this->addSearchQueryBuilder($queryBuilder, $search);
        }

        $queryBuilder = $this->addOrderByPublishedPostQueryBuilder($queryBuilder);
        $queryBuilder = $this->addOrderByPostQueryBuilder($queryBuilder);

        return (array) $queryBuilder
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string|null $pseudo
     * @param string|null $search
     *
     * @return array|null
     */
    public function getAllUnpublishedPostsByUserOrderByDesc(
        ?string $pseudo,
        ?string $search = null
    ): ?array {
        $queryBuilder = $this->createQueryBuilder('p');

        if ($pseudo) {
            $queryBuilder->join('p.user', 'u')
                ->andWhere("u.pseudo = '$pseudo'");
        }

        if ($search) {
            $queryBuilder = $this->addSearchQueryBuilder($queryBuilder, $search);
        }

        $queryBuilder = $this->addOrderByUnpublishedPostQueryBuilder($queryBuilder);
        $queryBuilder = $this->addOrderByPostQueryBuilder($queryBuilder);

        return (array) $queryBuilder
            ->getQuery()
            ->getResult();
    }

    private function addOrderByPostQueryBuilder($queryBuilder)
    {
        $queryBuilder = $queryBuilder ?? $this->createQueryBuilder('p');

        return $queryBuilder->orderBy('p.updatedAt', 'DESC');
    }

    private function addOrderByPublishedPostQueryBuilder($queryBuilder)
    {
        $queryBuilder = $queryBuilder ?? $this->createQueryBuilder('p');

        return $queryBuilder->andWhere('p.isPublished = true');
    }

    private function addOrderByUnpublishedPostQueryBuilder($queryBuilder)
    {
        $queryBuilder = $queryBuilder ?? $this->createQueryBuilder('p');

        return $queryBuilder->andWhere('p.isPublished = false');
    }

    private function addSearchQueryBuilder($queryBuilder, string $search)
    {
        $queryBuilder = $queryBuilder ?? $this->createQueryBuilder('p');

        return $queryBuilder->andWhere('p.content LIKE :searchTerm OR p.title LIKE :searchTerm')
            ->setParameter('searchTerm', '%'.$search.'%');
    }
}
