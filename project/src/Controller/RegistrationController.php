<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\UserAuthenticator;
use App\Service\JWTService;
use App\Service\SendEmailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

final class RegistrationController extends AbstractController
{
    protected const JWT = 'JWT';
    protected const ALG = 'HS256';
    protected const HEADER = [
        'typ' => 'JWT',
        'alg' => 'HS256',
    ];
    protected const EMAIL = 'no-reply@monblog.com';
    protected const HTMLTEMPLATE = 'emails/registration.html.twig';
    protected const MESSAGE = 'Activation de votre compte sur le site MonBlog';

    /**
     * @param Request                     $request
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param UserAuthenticatorInterface  $userAuthenticator
     * @param UserAuthenticator           $authenticator
     * @param EntityManagerInterface      $entityManager
     * @param SendEmailService            $mail
     * @param JWTService                  $jwt
     *
     * @return Response
     */
    #[Route('/inscription', name: 'security_register')]
    public function register(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        UserAuthenticatorInterface $userAuthenticator,
        UserAuthenticator $authenticator,
        EntityManagerInterface $entityManager,
        SendEmailService $mail,
        JWTService $jwt,
    ): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $plainPassword = $form->get('plainPassword')->getData();
            $plainPasswordConvert = \strval($plainPassword);
            $passWordHash = $userPasswordHasher->hashPassword(
                $user,
                $plainPasswordConvert
            );

            $user->setPassword($passWordHash);
            $user->setIsAgree(true);

            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'Vous avez reçu un Email de confirmation');

            $userId = $user->getId();
            $payload = compact('userId');
            $secret = $this->getSecret();
            $token = $jwt->generateToken(self::HEADER, $payload, $secret);

            $context = [
                'user' => $user,
                'token' => $token,
            ];

            $mail->sendMail(
                self::EMAIL,
                (string) $user->getEmail(),
                self::MESSAGE,
                self::HTMLTEMPLATE,
                $context
            );

            return $this->redirectToRoute('security_login');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @param string                 $token
     * @param UserRepository         $userRepository
     * @param EntityManagerInterface $em
     * @param JWTService             $jwt
     *
     * @return Response
     */
    #[Route('/verif/{token}', name: 'security_verify')]
    public function verifyUserWithToken(
        string $token,
        UserRepository $userRepository,
        EntityManagerInterface $em,
        JWTService $jwt,
    ): Response {
        $secret = $this->getSecret();
        if ($jwt->isValidToken($token) && !$jwt->isExpiredToken($token) && $jwt->isSignatureOfToken($token, $secret)) {
            $payload = $jwt->getPayload($token);

            $user = $userRepository->find($payload['userId']);

            if ($user && !$user->isIsVerified()) {
                $user->setIsVerified(true);
                $em->flush($user);
                $this->addFlash('success', 'Utilisateur activé');

                return $this->redirectToRoute('security_login');
            }
        }

        $this->addFlash('danger', 'Le token est invalide ou a expiré');

        return $this->redirectToRoute('security_login');
    }

    private function getSecret(): string
    {
        $result = $this->getParameter('app.jwtsecret');

        return \strval($result);
    }
}
