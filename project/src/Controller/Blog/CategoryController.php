<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Entity\Category;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Twig\Environment;
use Twig\Extra\String\StringExtension;

use function Symfony\Component\String\u;

class CategoryController
{
    /**
     * @param Environment $twig
     * @param Security    $security
     */
    public function __construct(
        private Environment $twig,
        private Security $security,
    ) {
        $this->twig->addExtension(new StringExtension());
    }

    /**
     * @param Category           $category
     * @param PostRepository     $postRepository
     * @param string             $slug
     * @param PaginatorInterface $paginator
     * @param Request            $request
     * @param UserRepository     $userRepository
     *
     * @return Response
     */
    #[Route('/auteur/categorie/{slug}', name: 'category_get_posts_of_category_of_user')]
    #[IsGranted('ROLE_USER')]
    public function getAllPostsOfCategoryBySlugForUser(
        Category $category,
        PostRepository $postRepository,
        string $slug,
        PaginatorInterface $paginator,
        Request $request,
        UserRepository $userRepository,
    ): Response {
        $user = $this->security->getUser();
        $userEmail = $user->getUserIdentifier();

        if ($userEmail) {
            $userId = $userRepository->findOneByEmail($userEmail);
        }

        $userPseudo = $userId->getPseudo();
        $slugMod = $slug ? u(str_replace('-', ' ', $slug))->title(true) : null;

        $queryBuilder = $postRepository->getAllPostsByCategoryAndPseudo((string) $slugMod, $userPseudo);

        $posts = $paginator->paginate(
            $queryBuilder,
            $request->query->getint('page', 1),
            8
        );

        return new Response($this->twig->render('pages/post/getAllPostsOfCategoryBySlug.html.twig', [
            'category' => $category,
            'posts' => $posts,
        ]));
    }

    /**
     * @param Category           $category
     * @param PostRepository     $postRepository
     * @param string             $slug
     * @param PaginatorInterface $paginator
     * @param Request            $request
     *
     * @return Response
     */
    #[Route('/categorie/{slug}', name: 'category_get_all_posts_of_category_by_slug')]
    public function getAllPostsOfCategoryBySlug(
        Category $category,
        PostRepository $postRepository,
        string $slug,
        PaginatorInterface $paginator,
        Request $request,
    ): Response {
        $slugMod = $slug ? u(str_replace('-', ' ', $slug))->title(true) : null;

        $queryBuilder = $postRepository->getAllPostsByCategoryOrderByDesc((string) $slugMod);

        $posts = $paginator->paginate(
            $queryBuilder,
            $request->query->getint('page', 1),
            8
        );

        return new Response($this->twig->render('pages/post/getAllPostsOfCategoryBySlug.html.twig', [
            'category' => $category,
            'posts' => $posts,
        ]));
    }
}
