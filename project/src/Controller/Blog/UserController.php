<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Entity\User;
use App\Repository\PostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Extra\String\StringExtension;

class UserController
{
    /**
     * @param Environment $twig
     */
    public function __construct(
        private Environment $twig,
    ) {
        $this->twig->addExtension(new StringExtension());
    }

    /**
     * @param PostRepository     $postRepository
     * @param string             $pseudo
     * @param PaginatorInterface $paginator
     * @param Request            $request
     * @param User               $user
     *
     * @return Response
     */
    #[Route('/user/{pseudo}', name: 'user_get_all_posts_of_user_by_pseudo')]
    public function getAllPostsOfUserByPseudo(
        PostRepository $postRepository,
        string $pseudo,
        PaginatorInterface $paginator,
        Request $request,
        User $user,
    ): Response {
        $queryBuilder = $postRepository->getAllPublishedPostsByUserOrderByDesc($pseudo);

        $posts = $paginator->paginate(
            $queryBuilder,
            $request->query->getint('page', 1),
            8
        );

        return new Response($this->twig->render('pages/post/getAllPostsOfUserByPseudo.html.twig', [
            'user' => $user,
            'posts' => $posts,
        ]));
    }
}
