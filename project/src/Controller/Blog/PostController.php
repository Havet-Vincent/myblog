<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Entity\Comment;
use App\Entity\Post;
use App\Form\CommentFormType;
use App\Form\PostFormType;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use App\Service\SendEmailService;
use App\Service\UploaderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Twig\Environment;
use Twig\Extra\String\StringExtension;

final class PostController
{
    private const EMAIL = 'admin@monblog.com';
    private const HTMLTEMPLATE = 'emails/comment.html.twig';
    private const MESSAGE = 'Commentaire en attente d\'approbation.';

    /**
     * @param Environment                   $twig
     * @param UrlGeneratorInterface         $router
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param Security                      $security
     * @param UploaderHelper                $uploaderHelper
     */
    public function __construct(
        private Environment $twig,
        private UrlGeneratorInterface $router,
        private AuthorizationCheckerInterface $authorizationChecker,
        private Security $security,
        private UploaderHelper $uploaderHelper,
    ) {
        $this->twig->addExtension(new StringExtension());
    }

    /**
     * @param PostRepository     $postRepository
     * @param PaginatorInterface $paginator
     * @param Request            $request
     *
     * @return Response
     */
    #[Route('/articles', name: 'post_get_all_posts')]
    public function getAllPosts(
        PostRepository $postRepository,
        PaginatorInterface $paginator,
        Request $request,
    ): Response {
        $queryBuilder = $postRepository->getAllPostsOrderByDateDesc($request->query->get('q'));
        $posts = $paginator->paginate(
            $queryBuilder,
            $request->query->getint('page', 1),
            8
        );

        return new response($this->twig->render('pages/post/getAllPosts.html.twig', [
            'posts' => $posts,
            $request->query->get('q'),
        ]));
    }

    /**
     * @param PostRepository     $postRepository
     * @param PaginatorInterface $paginator
     * @param Request            $request
     * @param UserRepository     $userRepository
     *
     * @return Response
     */
    #[Route('/articles/publies/auteur', name: 'post_get_published_posts_connected_user')]
    #[IsGranted('ROLE_USER')]
    public function getPostsConnectedUser(
        PostRepository $postRepository,
        PaginatorInterface $paginator,
        Request $request,
        UserRepository $userRepository,
    ): Response {
        $user = $this->security->getUser();
        $userEmail = $user->getUserIdentifier();

        if ($userEmail) {
            $userId = $userRepository->findOneByEmail($userEmail);
        }

        $userPseudo = $userId->getPseudo();

        $queryBuilder = $postRepository->getAllPublishedPostsByUserOrderByDesc(
            $userPseudo,
            $request->query->get('q')
        );

        $posts = $paginator->paginate(
            $queryBuilder,
            $request->query->getint('page', 1),
            8
        );

        return new response($this->twig->render('pages/post/getPublishedPostsConnectedUser.html.twig', [
            'posts' => $posts,
        ]));
    }

    /**
     * @param PostRepository     $postRepository
     * @param PaginatorInterface $paginator
     * @param Request            $request
     * @param UserRepository     $userRepository
     *
     * @return Response
     */
    #[Route('/articles/non-publies/auteur', name: 'post_get_unpublihed_posts_connected_user')]
    #[IsGranted('ROLE_USER')]
    public function getUnpublishedPostsConnectedUser(
        PostRepository $postRepository,
        PaginatorInterface $paginator,
        Request $request,
        UserRepository $userRepository,
    ): Response {
        $user = $this->security->getUser();
        $userEmail = $user->getUserIdentifier();

        if ($userEmail) {
            $userId = $userRepository->findOneByEmail($userEmail);
        }

        $userPseudo = $userId->getPseudo();

        $queryBuilder = $postRepository->getAllUnpublishedPostsByUserOrderByDesc(
            $userPseudo,
            $request->query->get('q')
        );

        $posts = $paginator->paginate(
            $queryBuilder,
            $request->query->getint('page', 1),
            8
        );

        return new response($this->twig->render('pages/post/getUnpublishedPostsConnectedUser.html.twig', [
            'posts' => $posts,
        ]));
    }

    /**
     * @param Request                $request
     * @param EntityManagerInterface $em
     * @param FormFactoryInterface   $formFactory
     * @param Session                $session
     *
     * @return Response
     */
    #[Route('/article/nouveau', name: 'post_create_post', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function createPost(
        Request $request,
        EntityManagerInterface $em,
        FormFactoryInterface $formFactory,
        Session $session,
    ): Response {
        $post = new Post();
        $user = $this->security->getUser();

        $form = $formFactory->create(PostFormType::class, $post)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = (object) $form->getData();
            $post->setUser($user);

            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('image')->getData();

            if ($imageFile) {
                $imageFileName = $this->uploaderHelper->uploadImage($imageFile);
                $post->setImage($imageFileName);
            }

            $em->persist($post);
            $em->flush();

            $session->getFlashBag()->add('success', 'Votre article a été créé avec succès.');

            return new RedirectResponse($this->router->generate('post_get_published_posts_connected_user'));
        }

        return new Response($this->twig->render('pages/post/createPost.html.twig', [
            'form' => $form->createView(),
        ]));
    }

    /**
     * @param Post                   $post
     * @param Request                $request
     * @param EntityManagerInterface $em
     * @param FormFactoryInterface   $formFactory
     * @param Session                $session
     *
     * @return Response
     */
    #[Route('/article/modification/{slug}', name: 'post_edit_post_by_slug', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function editPostBySlug(
        Post $post,
        Request $request,
        EntityManagerInterface $em,
        FormFactoryInterface $formFactory,
        Session $session,
    ): Response {
        if (!$this->authorizationChecker->isGranted('edit', $post)) {
            throw new AccessDeniedException();
        }

        $imageFileFolder = $post->getImage();

        $form = $formFactory->create(PostFormType::class, $post)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = (object) $form->getData();

            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('image')->getData();

            if ($imageFile) {
                if ($imageFileFolder !== null) {
                    $this->uploaderHelper->deleteImage($imageFileFolder);
                }
                $imageFileName = $this->uploaderHelper->uploadImage($imageFile);
                $post->setImage($imageFileName);
            }
            $em->persist($post);
            $em->flush();

            $session->getFlashBag()->add('success', 'Votre article a été modifié avec succès.');

            return new RedirectResponse($this->router->generate('post_get_published_posts_connected_user'));
        }

        return new Response($this->twig->render('pages/post/createPost.html.twig', [
            'form' => $form->createView(),
        ]));
    }

    /**
     * @param Post                   $post
     * @param FormFactoryInterface   $formFactory
     * @param Request                $request
     * @param EntityManagerInterface $em
     * @param Session                $session
     * @param SendEmailService       $mail
     *
     * @return Response
     */
    #[Route('/article/{slug}', name: 'post_get_post_by_slug')]
    public function getPostBySlug(
        Post $post,
        FormFactoryInterface $formFactory,
        Request $request,
        EntityManagerInterface $em,
        Session $session,
        SendEmailService $mail,
    ): Response {
        $comments = $post->getComments();
        $comment = new Comment();

        $form = $formFactory->create(CommentFormType::class, $comment)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setPost($post);
            $comment->setIsAgree(true);

            $em->persist($comment);
            $em->flush();

            $session->getFlashBag()->add('success', 'Votre commentaire est en attente de validation.');

            $context = [
                'pseudo' => $comment->getNickname(),
                'comment' => $comment->getContent(),
            ];

            $mail->sendMail(
                (string) $comment->getEmail(),
                self::EMAIL,
                self::MESSAGE,
                self::HTMLTEMPLATE,
                $context
            );

            return new RedirectResponse($this->router->generate(
                'post_get_post_by_slug',
                ['slug' => $post->getSlug()]
            ));
        }

        return new Response($this->twig->render('pages/post/getPostBySlug.html.twig', [
            'post' => $post,
            'comments' => $comments,
            'form' => $form->createView(),
        ]));
    }

    /**
     * @param Post                   $post
     * @param EntityManagerInterface $em
     * @param Session                $session
     *
     * @return Response
     */
    #[Route('/article/suppression/{slug}', name: 'post_delete_post_by_slug', methods: ['GET'])]
    #[IsGranted('ROLE_USER', message: 'L\'accès vous est interdit')]
    public function deletePostBySlug(
        Post $post,
        EntityManagerInterface $em,
        Session $session,
    ): Response {
        if (!$this->authorizationChecker->isGranted('delete', $post)) {
            throw new AccessDeniedException();
        }

        $imageFileFolder = $post->getImage();
        $this->uploaderHelper->deleteImage($imageFileFolder);

        $em->remove($post);
        $em->flush();

        $session->getFlashBag()->add('success', 'Votre article a été supprimé avec succès.');

        return new RedirectResponse($this->router->generate('post_get_published_posts_connected_user'));
    }
}
