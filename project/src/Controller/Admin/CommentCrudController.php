<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Comment;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CommentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Comment::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('email', 'Email utilisateur')
            ->onlyOnIndex()->setTextAlign('center');
        yield TextField::new('nickname', 'Pseudo')
            ->onlyOnIndex()->setTextAlign('center');
        yield TextEditorField::new('content', 'Contenu du commentaire')
            ->onlyOnIndex()->setTextAlign('center');
        yield BooleanField::new('isApproved', 'Approuvé (O/N)')
            ->onlyOnIndex()->setTextAlign('center');
        yield DateField::new('createdAt', 'Date')->setFormat('dd/MM/yyyy')
            ->onlyOnIndex()->setTextAlign('center');
        yield TextField::new('post', 'Titre de l\'article')
            ->onlyOnIndex()->setTextAlign('center');
        yield TextEditorField::new('post.content', 'Contenu de l\'article')
            ->onlyOnIndex()->setTextAlign('center');
    }

    public function createIndexQueryBuilder(
        SearchDto $searchDto,
        EntityDto $entityDto,
        FieldCollection $fields,
        FilterCollection $filters
    ): QueryBuilder {
        return parent::createIndexQueryBuilder(
            $searchDto,
            $entityDto,
            $fields,
            $filters
        )
            ->andWhere('entity.isApproved = :approved')
            ->setParameter('approved', true);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPageTitle(Crud::PAGE_INDEX, 'Liste des commentaires approuvés')
            ->showEntityActionsInlined()
            ->setDefaultSort(['createdAt' => 'DESC']);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_INDEX, Action::EDIT);
    }
}
