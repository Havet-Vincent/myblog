<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'app_admin')]
    #[IsGranted('ROLE_ADMIN')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration du blog');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard(
            'Tableau de bord',
            'fa fa-dashboard'
        );

        yield MenuItem::section('Catégories');
        yield MenuItem::linkToCrud(
            'Catégories',
            'fas fa-tags',
            Category::class
        );

        yield MenuItem::section('Articles');
        yield MenuItem::linkToCrud(
            'Articles publiés',
            'fas fa-file-text',
            Post::class
        )
            ->setController(PostCrudController::class);
        yield MenuItem::linkToCrud(
            'Articles non publiés',
            'fas fa-file-text',
            Post::class
        )
            ->setController(PostUnpublishedCrudController::class);

        yield MenuItem::section('Commentaires');
        yield MenuItem::linkToCrud(
            'Commentaires approuvés',
            'fas fa-file-text',
            Comment::class
        )
            ->setController(CommentCrudController::class);
        yield MenuItem::linkToCrud(
            'Commentaires non approuvés',
            'fas fa-file-text',
            Comment::class
        )
            ->setController(CommentUnApprovedCrudController::class);

        yield MenuItem::section('Utilisateurs');
        yield MenuItem::linkToCrud(
            'Utilisateurs',
            'fas fa-user',
            User::class
        );

        yield MenuItem::section('Le Site');
        yield MenuItem::linkToUrl(
            'Accueil',
            'fas fa-home',
            $this->generateUrl('home_get_the_last_ten_posts')
        );
    }

    public function configureAssets(): Assets
    {
        return parent::configureAssets()
            ->addWebpackEncoreEntry('admin');
    }

    public function configureActions(): Actions
    {
        return parent::configureActions()
            ->update(
                Crud::PAGE_INDEX,
                Action::EDIT,
                static function (Action $action) {
                    return $action->setIcon('fa fa-edit');
                }
            )
            ->update(
                Crud::PAGE_INDEX,
                Action::DELETE,
                static function (Action $action) {
                    return $action->setIcon('fa fa-trash');
                }
            );
    }
}
