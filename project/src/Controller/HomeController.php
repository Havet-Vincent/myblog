<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Extra\String\StringExtension;

class HomeController
{
    /**
     * @param Environment $twig
     */
    public function __construct(private Environment $twig)
    {
        $this->twig->addExtension(new StringExtension());
    }

    /**
     * @param PostRepository $postRepository
     *
     * @return Response
     */
    #[Route('/', name: 'home_get_the_last_ten_posts', methods: ['GET'])]
    public function getTheLastTenPosts(
        PostRepository $postRepository,
        Request $request,
    ): Response {
        $posts = $postRepository->getTheLastTenPostsOrderByDateDesc();

        return new Response($this->twig->render('pages/home/getTheLastTenPosts.html.twig', [
            'posts' => $posts,
            $request->query->get('q'),
        ]));
    }
}
