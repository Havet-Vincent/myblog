# MyBlog

[![pipeline status](https://gitlab.com/Havet-Vincent/myblog/badges/main/pipeline.svg)](https://gitlab.com/Havet-Vincent/myblog/-/commits/main)

Symfony 6.2.\* + Docker images (Nginx + PHP-FPM + Mariadb + PhpMyAdmin + Mailhog)

Symfony packages already installed :

-   symfony/profiler-pack
-   symfony/orm-pack
-   symfony/maker-bundle
-   symfony/var-dumper
-   symfony/mailer
-   friendsofphp/php-cs-fixer
-   phpstan/phpstan
-   phpstan/extension-installer
-   phpstan/phpstan-symfony
-   symfony/test-pack

## Getting started

Prerequisites :

1. If not already done

-   [Docker](https://www.docker.com/)
-   [Docker compose](https://docs.docker.com/compose/install/) (v2.10+)
-   [Make](https://www.gnu.org/software/make/)
-   [mkcert](https://github.com/FiloSottile/mkcert#installation)

2. To remove the Docker containers:

```bash
  make rm
```

3. To remove unused all

```bash
  make prune
```

4. Create a new file `.env` from `.env.dist` with desired variables

5. Create a new file `passwordreset.sql` from `passwordreset.dist` if you want it

## Run Locally with Make

Run to build fresh images

```bash
  make build
```

Run to display help

```bash
  make help
```

## Ready to use with

This docker compose provides you :

-   nginx
-   php
-   mariadb
-   phpmyadmin
-   mailhog

List containers

```bash
  docker compose ps
```

## Requirements

Out of the box, this docker compose is designed for a Linux operating system, provide adaptations for a Mac or Windows environment.

-   Linux (Ubuntu 20.04 or other) (cat /etc/\*release)
-   Docker (v20.10+) (docker --version)
-   Docker compose (v2.10+) (docker compose version)
-   mkcert (create and install a local CA in the system root store...)

## Stop Locally

Stop running containers without removing them.

```bash
  make stop
```

Stop running containers and delete them

```bash
  make rm
```

## Credits

Created by Vincent Havet : [GitLab](https://gitlab.com/Havet-Vincent), [GitHub](https://github.com/Havet-Vincent).
